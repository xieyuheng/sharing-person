<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML>
<HEAD>
   <TITLE>Formalizing Mathematics Research Page</TITLE>
   </HEAD>
<BODY>

<H1>Formalizing Mathematics</H1> 
<H2>General</H2>

<B>What?</B> We are interested in formalizing mathematics on a
computer. The concrete aim is to make formalizing of known mathematics
of the same degree of easiness as writing it up in LaTeX. In addition,
we want the formalized mathematics to be available in an <I>active</I>
form, that represents also the semantics. So, we want to be able to
<UL>
<LI><I> browse </I> and <I> search </I> libraries of formalized
mathematics, (so it should both be readable, like a mathematics book
and searchable like a database), </LI>
<LI> <I>use</I> the stored formalized mathematics, e.g. by computing
with it or proving with it (so it should be possible to actively use
the mathematics in a computer algebra system or a theorem prover),
<LI> <I>extend</I> the corpus of formalized mathematics. For example by
extending the formalized theory or by adding an illuminating example. 
Extending should be as easy as writing it up in, say, LaTeX.
</UL>
<P>

<B>Why?</B> There is a lot of mathematical knowledge. This knowledge
is mainly stored in books and in the heads of mathematicians and other
scientists. Putting this knowledge in the right form on a computer,
the mathematics should be more readily available to be used by others
(either humans or other computer applications).  In this respect, a
positive thing about mathematical knowledge is that it has a rather
formal (and precise) nature, which makes it easier to formalize.
<P>

<B>How?</B> We perform and study concrete (large) formalizations of
mathematics in mathematical assistants. Presently there are two types
of system in which mathematics can be presented in some kind of
semantical form: Theorem Provers (TPs) and Computer Algebra Systems
(CAs). The first have the advantage that a lot of mathematical
concepts can be represented (defined) very precisely and hence
completely formalized proofs can be given. The second have the
advantage that it is much simpler to represent mathematics (if it
falls within the expressive scope of the CA) and its computation
capabilities are much larger.  
<P>
At present, TPs are the only systems in which one can really formalize
a large piece of mathematics, so we focus on those.  We are mainly
users of <A HREF ="http://www.inria.fr/coq">Coq</A> and we have
experimented (and we presently still are) with some larger
developments in Coq. We also develop specific tools for supporting and
automating the proof-process. Other systems that we look into are <A
HREF ="http://www.mizar.com/">Mizar</A> and <A HREF
="http://www.hol.com">HOL-light</A> (but this list may vary from time
to time). We have also done some experiments with combining TPs and
CAs, to let the TP profit from the computing facilities of the
CA. Another topic that we study is the presentation of formalized
mathematics. Ideally this should be done via an (interactive) document
that can be read roughly as an ordinary mathematics book.

<H2>People</H2>

The following people in Nijmegen are involved in the research on
Formalizing Mathematics.
<UL>
<LI><A HREF = "http://www.cs.kun.nl/~herman">Herman Geuvers</A></LI>
<LI><A HREF = "http://www.cs.kun.nl/~henk">Henk Barendregt</A></LI>
<LI><A HREF = "http://www.cs.kun.nl/~freek">Freek Wiedijk</A></LI>
<LI><A HREF = "http://www.cs.kun.nl/~synek">Dan Synek</A></LI>
<LI><A HREF = "http://www.cs.kun.nl/~venanzio">Venanzio Capretta</A></LI>
<LI><A HREF = "http://www.cs.kun.nl/~milad">Milad Niqui</A></LI>
<LI><A HREF = "http://www.cs.kun.nl/~jasper">Jasper Stein</A></LI>
<LI><A HREF = "http://www.cs.kun.nl/~lcf">Luis Cruz Filipe</A></LI>
</UL>

<H2>Projects</H2>
<UL>
<LI> <B>The Fundamental Theorem of Algebra (FTA) project.</B> This
comprises the full formalization in Coq of a constructive proof of the
Fundamental Theorem of Algebra (every non-trivial polynomial over the
complex numbers has a root). Details can be found at the <A HREF =
"http:/www.cs.kun.nl/~fta">FTA page</A>. The formalization has been
finished by November 2000. We are still building on top of this
development and we will make sure that it remains compatible with new
versions of Coq.
</LI>
<LI> <B>Formalizing the real numbers in Coq.</B> This started out as a
subproject of FTA: give a complete construction of the reals in Coq.
Now we are interested in `real' representations of the reals (i.e. as
infinitary objects), the algorithms one can represent over them and
the properties that can be proved about them. One of the challenges is
to program (and prove correct) actual functions over the reals, within
Coq.
</LI>
<LI><B>Linear Algebra in Coq.</B> Represent (abstract) notions like vector
space, linear transformation, etcetera and construct (concrete)
instances of these, like IRxIR, matrix, etcetera. The challenge lies
in the connection between abstract structures and their concrete
instances.</LI>

<LI><B>Analysis in Coq.</B> Based on the FTA files, define notions like
differentiation and integration for real (or complex) valued
functions. As TPs are generally more geared towards abstract reasoning
than concrete epsilon-delta reasoning (involving many `simple'
computation steps), this is a real challenge.
</LI>

<LI><B> Four Color Theorem in HOL-light.</B> The four color theorem is
a well-known `difficult proof' in mathematics. Various faulty proofs
have been given in the 19th and 20th century and the actual (widely
accepted) proof consists of a large set of algorithms that all have to
be proved correct and executed to obtain the final result. The
formalization of this proof is an obvious challenge, because it really
requires a lot of computation and also because not all mathematicians
accept the existing proof as a `real proof'.

<LI><B> User Manual for Mizar.</B> Mizar is the oldest (still working)
system for theorem proving. It has an impressive library of
mathematical results, which is still actively extended. Particular
features of the system are that it is batch oriented (not interactive)
and uses a `declarative' proof style which is in spirit quite close to
ordinary mathematical text. A problem of the system is that it has no
user manual, so it is hard to learn and therefore quite under-valued.
</LI>

<LI><B>Formalizing Partial Functions.</B> In ordinary (say first
order) logic, functions are viewed as single-valued relations. This
follows the standard set-theoretic approach and it generalises
straightforwardly to partial functions. In formal mathematics, one
would like the functions to be actual <I>operations</I> (or even
<I>algorithms</I>), allowing to just write f(x) instead of talking
about `the y for which R(x,y) holds'. This does not so easily
generalize to partial functions, as it is not clear what f(x) means if
x is not in the domain of f: is it an illegal expression? or is it
legal, but can we just not prove anything about it?
</LI>

<LI> <B>Combining Coq with a CAs.</B> A first phase of this
project is finished, yielding an implementation of a primality checker
in Coq, which uses GAP to yield `witnesses'. The algorithm used is based
on Pocklington's Criterion, which requires the factorization of large
numbers. These factorizations -- which are hard to <I>find</I> in Coq --
are asked to GAP and then <I>checked</I> in Coq. There are more ways
in which CAs can be used within a TP, which are still to be exploited.
</LI>

<LI><B>Presentation of formalized mathematics.</B> Given a piece of
formalized mathematics, we want to present it to the world, e.g. to
mathematicians to evaluate it or to students to build on top of it.
The actual computer code is very system specific and contains far too
much detail to access the material. On the other hand, an
`accompanying paper' in the standard mathematical style does not show
sufficient detail of what has actually been formalized (and may not at
all be a faithful representation of the formalization). Coq has a way
of generating natural language from a formalized proof. It is also
possible to generate system independent encodings (OMDoc, XML) from
Coq proofs. A different angle is taken by Mizar, which tries to
keep the proof files themselves close to `ordinary mathematics'. None
of these approaches have so far led to a satisfactory solution.
</LI>
</UL>

<H2>Results</H2>


<HR WIDTH="100%"></H3>

</BODY>
</HTML>




