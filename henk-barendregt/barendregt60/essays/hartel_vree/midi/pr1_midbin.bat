REM This batch file composes and plays the music published in our paper
REM You should replace the path to runhugs.exe with your specific installation
REM The same for the midi player
REM You could try XMPlay, which has a good software midi renderer that reads
REM soundfont files: http://support.xmplay.com/

F:\Software\Tools\Hugs\runhugs.exe pr1_midbin_04.hs 160 1 7 6 1
F:\Muziek\XMPlay\xmplay.exe zmid.mid
